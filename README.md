# NYC Schools App

Dive into the NYC High Schools app, a comprehensive guide to New York City's educational landscape, designed with SwiftUI for an intuitive user experience. This app simplifies the search for high schools with powerful features:

- **Search Functionality**: Quickly find schools by name or neighborhood.
- **Sorting Capabilities**: Organize school listings alphabetically or by student population. Offers a dedicated sorting class adhering to SOLID principles for better separation of concerns.
- **Detailed School Profiles**: Access in-depth information, including contact details and academic statistics, in a user-friendly format.
- **Localization**: Experience the app in your language with full support for internationalization.

## Demo
![High School App Demo gif](HighSchoolApp-Demo.gif)

High School App Demo video path -> HighSchoolApp-Demo.mov

## Architecture Highlights

- **MVVM & SOLID Principles**: Promotes separation of concerns and testability.
- **SwiftUI Views**: Leverages SwiftUI for a modern UI experience.
- **Observable Objects**: `SchoolListViewModel` and `SATDetailViewModel` for state management.
- **Custom Modifiers**: Reusable SwiftUI modifiers like `CardContainerView` for consistent styling.
- **Networking Abstraction**: Adheres to SchoolDataServiceProtocol for testable networking, employing protocol-oriented programming for modularity and decoupling.
- **Service Factory & Singleton Access**: Implements a Service Factory for centralized service instantiation and provides singleton access to core services via AppEnvironment, ensuring controlled and consistent service management.
- **Asynchronous Networking**: Ensures UI responsiveness with asynchronous network calls, handling data fetching and processing on background threads with synchronization to the main thread for UI updates.
- **Error Handling & Thread Safety**: Features detailed error handling with a custom NetworkError type and guarantees thread safety, preventing race conditions and ensuring a smooth user experience.

## Testing

- **Unit Tests- XCTest**: Validate business logic and model integrity.
- **UI Tests- XCUITest**: Automated XCUITests to ensure UI consistency and reliability.

## Installation

Instructions for cloning, building, and running the app, including prerequisites.

```bash
git clone https://gitlab.com/ios9303445/20231106-AmitChandel-NYCSchools.git
open HighSchoolHub NYC.xcodeproj
