//
//  Double.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

extension Double {
    // Helper function to convert a Double to a percentage string rounded to the nearest whole number
    func roundedPercentage() -> String {
        let roundedValue = (self * 100).rounded()
        return "\(Int(roundedValue))%"
    }
}
