//
//  Array.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

// Helper extension to determine if the current item is the last in the list
extension Array where Element: Identifiable {
    func isLastItem(_ item: Element) -> Bool {
        self.last?.id == item.id
    }
}
