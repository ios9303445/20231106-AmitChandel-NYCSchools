//
//  Color.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

extension Color {
    static let navigationBarColor = Color("NavigationBarColor")
    static let rowBackgroundColor = Color("RowBackgroundColor")
    static let textColor = Color("TextColor")
    static let cardBackgroundColor = Color("CardBackgroundColor")
}
