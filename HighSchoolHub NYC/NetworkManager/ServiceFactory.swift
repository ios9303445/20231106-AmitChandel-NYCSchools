//
//  ServiceFactory.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

/*
  ServiceFactory.swift
  Factory class responsible for creating instances of service objects.
  Follows the Factory pattern to encapsulate the creation logic of service objects used throughout the app.
 */
import Foundation

class ServiceFactory {
    private let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
}

extension ServiceFactory {
    func makeSchoolDataService() -> SchoolDataServiceProtocol {
        return SchoolDataService(networkService: networkService)
    }
    
    // Placeholder for future data service creation methods.
}
