//
//  NetworkService.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

/*
  NetworkService.swift
  Conforms to NetworkServiceProtocol, providing a concrete implementation of network calls using URLSession.
  Part of the Service Layer in a Clean Architecture pattern, it handles fetching data from the network.
 */
protocol NetworkServiceProtocol {
    func fetchData<T: Decodable>(from endpoint: String, completion: @escaping (Result<T, NetworkError>) -> Void)
}

class NetworkService: NetworkServiceProtocol {
    func fetchData<T: Decodable>(from endpoint: String, completion: @escaping (Result<T, NetworkError>) -> Void) {
        guard let url = URL(string: endpoint) else {
            completion(.failure(NetworkError.badURL))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let _ = error {
                completion(.failure(NetworkError.badURL))
                return
            }
            guard let data = data else {
                completion(.failure(NetworkError.badResponse(statusCode: (response as? HTTPURLResponse)?.statusCode ?? 0)))
                return
            }
            do {
                let decodedData = try JSONDecoder().decode(T.self, from: data)
                completion(.success(decodedData))
            } catch {
                completion(.failure(NetworkError.parsing(error as? DecodingError)))
            }
        }.resume()
    }
}
