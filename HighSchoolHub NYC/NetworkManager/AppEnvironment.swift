//
//  AppEnvironment.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

/*
  AppEnvironment.swift
  Singleton class that holds the app's global dependencies, such as service factories.
  Implements the Singleton pattern to provide a globally accessible environment object.
 */
import Foundation

class AppEnvironment: ObservableObject {
    static var shared: AppEnvironment!

    let serviceFactory: ServiceFactory
    
    init(serviceFactory: ServiceFactory) {
        self.serviceFactory = serviceFactory
    }
}
