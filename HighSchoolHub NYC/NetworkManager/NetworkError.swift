//
//  NetworkError.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

/*
  `NetworkError` encapsulates all possible errors in the networking layer.
  This approach centralizes error handling, making the code more maintainable and the user experience more consistent.
 */
enum NetworkError: Error, CustomStringConvertible {
    case badURL
    case badResponse(statusCode: Int)
    case parsing(DecodingError?)
    case unknown
    
    var localizedDescription: String {
        switch self {
        case .badURL, .badResponse(statusCode: _), .parsing:
            return "Something went wrong"
        case .unknown:
            return "Sorry, connection to our server failed"
        }
    }
    
    var description: String {
        switch self {
        case .badURL:
            return "Invalid URL"
        case .badResponse(statusCode: let statusCode):
            return "Bad response with status code \(statusCode)"
        case .parsing(let error):
            return "Parsing error \(error?.localizedDescription ?? "")"
        case .unknown:
            return "Unknown error"
        }
    }
}
