//
//  CardContainerView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

let globalCardWidth = UIScreen.main.bounds.width - 32

// Reusable card view container for card styling
struct CardContainerView: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .frame(maxWidth: globalCardWidth)
            .background(Color.cardBackgroundColor)
            .cornerRadius(12)
            .shadow(color: Color.black.opacity(0.1), radius: 5, x: 0, y: 2)
            .padding(.horizontal)
    }
}

// Extension to easily apply the card style
extension View {
    func cardStyle() -> some View {
        self.modifier(CardContainerView())
    }
}
