//
//  HighSchoolHub_NYCApp.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import SwiftUI

@main
struct HighSchoolHub_NYCApp: App {
    
    let appEnvironment: AppEnvironment
    
    init() {
        let networkService = NetworkService()
        let serviceFactory = ServiceFactory(networkService: networkService)
        self.appEnvironment = AppEnvironment(serviceFactory: serviceFactory)
        
        customizeNavigationBar()
    }
    
    var body: some Scene {
        WindowGroup {
            SchoolListView(
                viewModel: SchoolListViewModel(
                    schoolDataService: appEnvironment.serviceFactory.makeSchoolDataService(),
                    schoolSorter: SchoolListSorter()
                )
            )
            .environmentObject(appEnvironment)
        }
    }
    
    func customizeNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "NavigationBarColor")
        appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        
        // Set the default navigation bar appearance
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        // For large titles
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
        // Set the tint color for navigation bar items
        UINavigationBar.appearance().tintColor = UIColor.white
    }
}
