//
//  SchoolListViewStrings.swift
//  HighSchoolHub NYC
//
//  Created by Prateek Masali on 11/5/23.
//

import Foundation

import Foundation

struct SchoolListViewStrings {
    static var navigationTitle: String {
        return NSLocalizedString("NavigationTitle", comment: "Title for the navigation bar")
    }
    
    static var searchSchoolsPrompt: String {
        return NSLocalizedString("SearchSchoolsPrompt", comment: "Prompt for the search bar")
    }
    
    static var sortAlphabetically: String {
        return NSLocalizedString("SortAlphabetically", comment: "Sort option for alphabetical")
    }
    
    static var sortByTotalStudents: String {
        return NSLocalizedString("SortByTotalStudents", comment: "Sort option for total students")
    }
}
