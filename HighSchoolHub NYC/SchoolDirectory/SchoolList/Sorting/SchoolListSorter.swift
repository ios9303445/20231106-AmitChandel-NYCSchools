//
//  SchoolListSorter.swift
//  HighSchoolHub NYC
//
//  Created by Prateek Masali on 11/7/23.
//

import Foundation

/*
   Adhering to the Single Responsibility Principle by taking responsibility
   for sorting logic only. Relatively cleaner way to add more sorting functionalities in the future. It also follows the principle of immutability by not mutating
   the input array directly but instead returning a new sorted array.
 */
enum SortOption {
    case alphabetical
    case totalStudents
}

protocol SchoolListSorterProtocol {
    func sort(schools: [School], by option: SortOption) -> [School]
}

class SchoolListSorter: SchoolListSorterProtocol {
    func sort(schools: [School], by option: SortOption) -> [School] {
        var sortedSchools = schools

        switch option {
        case .alphabetical:
            sortedSchools.sort { $0.schoolName < $1.schoolName }
        case .totalStudents:
            sortedSchools.sort {
                if let students1 = Int($0.totalStudents), let students2 = Int($1.totalStudents) {
                    return students1 > students2
                }
                return $0.schoolName < $1.schoolName
            }
        }
        return sortedSchools
    }
}
