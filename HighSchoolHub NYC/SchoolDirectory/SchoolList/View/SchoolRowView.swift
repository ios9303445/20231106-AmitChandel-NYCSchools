//
//  SchoolRowView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

struct SchoolRowView: View {
    let school: School
    
    var body: some View {
        HStack {
            Image(systemName: "graduationcap")
                .foregroundColor(.primary)
                .font(.system(size: 20))
                .padding(.trailing, 5)
            VStack(alignment: .leading, spacing: 5) {
                Text(school.schoolName)
                    .font(.headline)
                    .foregroundColor(.primary)
                Text(school.neighborhood)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                HStack {
                    Image(systemName: "phone.fill")
                        .foregroundColor(.secondary)
                    Text(school.phoneNumber)
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
            }
        }
        .accessibility(identifier: "SchoolRow_\(school.id)")
        .padding(.horizontal)
    }
}
