//
//  SchoolListView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

// MARK: - SchoolListView
struct SchoolListView: View {
    @ObservedObject var viewModel: SchoolListViewModel
    @EnvironmentObject var appEnvironment: AppEnvironment
    @State private var searchText: String = ""
    
    init(viewModel: SchoolListViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                schoolList
                if viewModel.isLoading {
                    LoadingView()
                }
            }
            .navigationTitle(SchoolListViewStrings.navigationTitle)
            .searchable(text: $searchText, prompt: SchoolListViewStrings.searchSchoolsPrompt)
            .toolbar { SortButton }
            .onAppear {
                if viewModel.schoolList.isEmpty {
                    viewModel.loadSchools()
                }
            }
        }
    }
}

// MARK: - Subviews
private extension SchoolListView {
    var SortButton: some View {
        Menu {
            Button(SchoolListViewStrings.sortAlphabetically, action: { viewModel.sortOption = .alphabetical; viewModel.sortSchools() })
            Button(SchoolListViewStrings.sortByTotalStudents, action: { viewModel.sortOption = .totalStudents; viewModel.sortSchools() })
        } label: {
            Image(systemName: "arrow.up.arrow.down")
        }
    }
    
    var schoolList: some View {
        List(filteredSchools) { school in
            NavigationLink(destination: SATDetailView(school: school, viewModel: SATDetailViewModel(schoolDataService: viewModel.schoolDataService))) {
                SchoolRowView(school: school)
            }
            .accessibility(identifier: "SchoolRow_\(school.id)")
            .onAppear {
                if viewModel.schoolList.isLastItem(school) {
                    viewModel.loadSchools()
                }
            }
            .listRowInsets(EdgeInsets(top: 8, leading: 10, bottom: 10, trailing: 15))
        }
        .accessibility(identifier: "SchoolList")
    }
    
    var filteredSchools: [School] {
        if searchText.isEmpty {
            return viewModel.schoolList
        } else {
            return viewModel.schoolList.filter {
                $0.schoolName.localizedCaseInsensitiveContains(searchText) ||
                $0.neighborhood.localizedCaseInsensitiveContains(searchText)
            }
        }
    }
}

// MARK: - LoadingView
private struct LoadingView: View {
    var body: some View {
        ProgressView()
            .scaleEffect(1.5)
            .progressViewStyle(.circular)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.black.opacity(0.5).edgesIgnoringSafeArea(.all))
            .accessibility(identifier: "LoadingProgressView")
    }
}
