//
//  School.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

struct School: Decodable, Identifiable, Equatable {
    
    let id: String
    let schoolName: String
    let website: String
    let neighborhood: String
    let phoneNumber: String
    let totalStudents: String
    let overviewParagraph: String
    let schoolEmail: String?
    let academicOpportunities1: String?
    let academicOpportunities2: String?
    let extraCurricularActivities: String?
    let graduationRate: String?
    let attendanceRate: String?
    let schoolSports: String?
    let offerRate: String?
    let location: String
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case schoolName = "school_name"
        case website
        case neighborhood
        case phoneNumber = "phone_number"
        case totalStudents = "total_students"
        case overviewParagraph = "overview_paragraph"
        case schoolEmail = "school_email"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case extraCurricularActivities = "extracurricular_activities"
        case graduationRate = "graduation_rate"
        case attendanceRate = "attendance_rate"
        case schoolSports = "school_sports"
        case offerRate = "offer_rate1"
        case location = "location"
    }
    
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.id == rhs.id
    }
}

extension School {
    
    // Computed property to get the website URL with "https://" prefix
    var fullWebsiteURL: String {
        if website.hasPrefix("http://") || website.hasPrefix("https://") {
            return website
        } else {
            return "https://\(website)"
        }
    }
    
    // Computed graduationRate in percentage
    var graduationRateInPercent: String {
        if let rateString = graduationRate, let rate = Double(rateString) {
            return rate.roundedPercentage()
        }
        return "N/A"
    }

    // Computed attendanceRate in percentage
    var attendanceRateInPercent: String {
        if let rateString = attendanceRate, let rate = Double(rateString) {
            return rate.roundedPercentage()
        }
        return "N/A"
    }
}
