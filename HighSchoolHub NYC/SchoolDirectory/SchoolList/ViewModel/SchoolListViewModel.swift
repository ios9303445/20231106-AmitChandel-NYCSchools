//
//  SchoolListViewModel.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

// MARK: - LoadingView
class SchoolListViewModel: ObservableObject {
    @Published var schoolList: [School] = []
    @Published var isLoading: Bool = false
    @Published var errorMessage: String?
    @Published var sortOption: SortOption = .alphabetical

    let schoolDataService: SchoolDataServiceProtocol
    let schoolSorter: SchoolListSorterProtocol

    init(schoolDataService: SchoolDataServiceProtocol, schoolSorter: SchoolListSorterProtocol) {
        self.schoolDataService = schoolDataService
        self.schoolSorter = schoolSorter
    }
}

// MARK: - Load & Sort schools
extension SchoolListViewModel {
    func loadSchools() {
        isLoading = true
        errorMessage = nil
        schoolDataService.fetchSchools { [weak self] result in
            DispatchQueue.main.async {
                self?.isLoading = false
                switch result {
                case .success(let schoolList):
                    self?.schoolList = schoolList
                    self?.sortSchools()
                case .failure(let error):
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
    }
    
    func sortSchools() {
        schoolList = schoolSorter.sort(schools: schoolList, by: sortOption)
    }
}
