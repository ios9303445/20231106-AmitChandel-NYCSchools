//
//  SATDetailView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

// MARK: - SATDetailView
struct SATDetailView: View {
    let school: School
    @ObservedObject var viewModel: SATDetailViewModel

    init(school: School, viewModel: SATDetailViewModel) {
        self.school = school
        self.viewModel = viewModel
    }

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 16) {
                SATScoreSection
                StudentStatisticsSection
                SchoolDetailsSection
            }
            .navigationBarTitleDisplayMode(.inline)
            .onAppear {
                viewModel.fetchSATScore(forSchool: school.id)
            }
            .padding()
        }
        .safeAreaInset(edge: .top, alignment: .leading, spacing: 0) {
            SATHeaderView(school: school)
        }
    }
}

// MARK: - Subviews
private extension SATDetailView {
    var SATScoreSection: some View {
        Group {
            if let satDetails = viewModel.schoolSATDetail {
                SATScoreView(satScore: satDetails)
            } else if viewModel.errorMessage != nil {
                Text(SATDetailViewStrings.errorLoadingMessage)
                    .foregroundColor(.red)
                    .fontWeight(.bold)
            } else {
                ProgressView(SATDetailViewStrings.loadingMessage)
            }
        }
    }

    var StudentStatisticsSection: some View {
        StudentStatisticsView(
            totalStudents: school.totalStudents,
            graduationRate: school.graduationRateInPercent,
            attendanceRate: school.attendanceRateInPercent
        )
        .padding(.vertical)
    }

    var SchoolDetailsSection: some View {
        SchoolDetailsView(school: school)
    }
}
