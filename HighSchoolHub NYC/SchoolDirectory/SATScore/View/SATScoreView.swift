//
//  SATScoreView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

// MARK: - SATScoreView
struct SATScoreView: View {
    let satScore: SATScore

    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            Text("SAT Scores")
                .font(.title2)
                .foregroundColor(Color.textColor)
                .fontWeight(.semibold)
                .padding(.vertical, 5)
            
            ScoreDetailView(title: "Math Score", score: satScore.mathScore, iconName: "function")
            ScoreDetailView(title: "Reading Score", score: satScore.readingScore, iconName: "book")
            ScoreDetailView(title: "Writing Score", score: satScore.writingScore, iconName: "pencil")
        }
        .cardStyle()
    }
}

// MARK: - ScoreDetailView
struct ScoreDetailView: View {
    var title: String
    var score: String
    var iconName: String

    var body: some View {
        HStack {
            Image(systemName: iconName)
                .foregroundColor(.accentColor)
                .frame(width: 30)
            Text(title)
                .font(.headline)
                .foregroundColor(.secondary)
            Spacer()
            Text(score)
                .font(.title2)
                .fontWeight(.bold)
        }
        .padding(.vertical, 2)
    }
}
