//
//  ContactGroupView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/6/23.
//

import Foundation
import SwiftUI

// MARK: - ContactGroupView
struct ContactGroupView: View {
    let phoneNumber: String
    let email: String?
    let website: String?

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Contact Information")
                .font(.title2)
                .fontWeight(.semibold)
                .foregroundColor(Color.textColor)
                .padding(.vertical, 5)

            if let phoneURL = URL(string: "tel://\(phoneNumber)") {
                Link(destination: phoneURL) {
                    ContactItemView(icon: "phone.fill", text: phoneNumber)
                }
            }

            if let email = email, let emailURL = URL(string: "mailto:\(email)") {
                Link(destination: emailURL) {
                    ContactItemView(icon: "envelope.fill", text: email)
                }
            }

            if let website = website, let websiteURL = URL(string: website) {
                Link(destination: websiteURL) {
                    ContactItemView(icon: "globe", text: "Go to Website")
                }
            }
        }
        .cardStyle()
    }
}

// MARK: - ContactItemView
struct ContactItemView: View {
    let icon: String
    let text: String

    var body: some View {
        HStack {
            Image(systemName: icon)
                .foregroundColor(.accentColor)
                .imageScale(.large)
            Text(text)
                .foregroundColor(Color.textColor)
            Spacer()
        }
        .padding(.vertical, 2)
    }
}
