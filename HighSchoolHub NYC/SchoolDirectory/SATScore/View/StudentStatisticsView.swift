//
//  StudentStatisticsView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

struct StudentStatisticsView: View {
    let totalStudents: String
    let graduationRate: String
    let attendanceRate: String
    
    var body: some View {
        VStack {
            Divider()
            HStack(spacing: 20) {
                
                // Students
                VStack(alignment: .center, spacing: 4) {
                    Text("Students")
                        .font(.headline)
                        .foregroundColor(.secondary)
                    Text(totalStudents)
                        .font(.title2)
                }
                .padding(.vertical, 8)
                Spacer()

                // Attendance Rate
                VStack(alignment: .center, spacing: 4) {
                    Text("Attendance")
                        .font(.headline)
                        .foregroundColor(.secondary)
                    Text(attendanceRate)
                        .font(.title2)
                }
                .padding(.vertical, 8)
                Spacer()

                // Graduation Rate
                VStack(alignment: .center, spacing: 4) {
                    Text("Graduation")
                        .font(.headline)
                        .foregroundColor(.secondary)
                    Text(graduationRate)
                        .font(.title2)
                }
                .padding(.vertical, 8)
            }
            .frame(minHeight: 80)
            Divider()
        }
    }
}
