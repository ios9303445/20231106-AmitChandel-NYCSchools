//
//  SATHeaderView.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation
import SwiftUI

struct SATHeaderView: View {
    let school: School

    var body: some View {
        VStack(spacing: 0) {
            Text(school.schoolName)
                .font(.system(.title, design: .rounded))
                .foregroundColor(.primary)
                .lineLimit(2)
                .multilineTextAlignment(.center)
                .minimumScaleFactor(0.5)
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.horizontal)
                .padding(.vertical, 8)

            HStack(spacing: 2) { 
                Image(systemName: "location.fill")
                    .foregroundColor(.secondary)
                Text(school.neighborhood)
                    .font(.title2)
                    .foregroundColor(.secondary)
            }

            Divider()
        }
        .background(Material.thick)
    }
}
