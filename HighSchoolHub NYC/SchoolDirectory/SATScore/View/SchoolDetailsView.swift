import Foundation
import SwiftUI

// MARK: - SchoolDetailsView
struct SchoolDetailsView: View {
    let school: School

    var body: some View {
        VStack(alignment: .leading, spacing: 16) {
            ContactGroupView(
                phoneNumber: school.phoneNumber,
                email: school.schoolEmail,
                website: school.fullWebsiteURL
            )
            
            DetailSectionView(title: "Extracurricular Activities", details: school.extraCurricularActivities)
            DetailSectionView(title: "Sports", details: school.schoolSports ?? "N/A")
            AcademicOpportunitiesView(academicOpportunities1: school.academicOpportunities1, academicOpportunities2: school.academicOpportunities2)
            
            DetailView(title: "Offer Rate", detail: school.offerRate ?? "N/A")
            DetailView(title: "Location", detail: school.location)
            OverviewSection(overviewParagraph: school.overviewParagraph)
        }
    }
}

// MARK: - AcademicOpportunitiesView
private struct AcademicOpportunitiesView: View {
    let academicOpportunities1: String?
    let academicOpportunities2: String?

    var body: some View {
        DetailView(
            title: "Academic Opportunities",
            detail: [academicOpportunities1, academicOpportunities2]
                .compactMap { $0 }
                .joined(separator: "\n")
                .ifEmpty("N/A")
        )
    }
}

// MARK: - OverviewSection
private struct OverviewSection: View {
    let overviewParagraph: String

    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("Overview")
                .font(.headline)
                .foregroundColor(.secondary)
            Text(overviewParagraph)
                .padding(.vertical, 4)
                .cardStyle()
        }
    }
}

// MARK: - DetailSectionView
struct DetailSectionView: View {
    let title: String
    let details: String?
    
    var detailsArray: [String] {
        details?.components(separatedBy: ", ") ?? []
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(title)
                .font(.headline)
                .foregroundColor(.secondary)
            
            HorizontalDetailsList(detailsArray: detailsArray)
        }
    }
}

// MARK: - HorizontalDetailsList
private struct HorizontalDetailsList: View {
    let detailsArray: [String]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(detailsArray, id: \.self) { detail in
                    Text(detail)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 14)
                        .font(.body)
                        .background(Color.gray.opacity(0.2))
                        .cornerRadius(8)
                        .fixedSize(horizontal: false, vertical: true)
                }
            }
        }
    }
}

// MARK: - DetailView
struct DetailView: View {
    var title: String
    var detail: String

    var body: some View {
        VStack(alignment: .leading, spacing: 4) {
            Text(title)
                .font(.headline)
                .foregroundColor(.secondary)
            Text(detail)
                .font(.body)
        }
        .padding(.vertical, 4)
    }
}

// MARK: - String Extension for Empty Check
private extension String {
    func ifEmpty(_ substitute: String) -> String {
        self.isEmpty ? substitute : self
    }
}
