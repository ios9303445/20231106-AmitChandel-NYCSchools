//
//  SATDetailViewModel.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

class SATDetailViewModel: ObservableObject {
    @Published var schoolSATDetail: SATScore?
    @Published var errorMessage: String?
    
    private let schoolDataService: SchoolDataServiceProtocol
    
    init(schoolDataService: SchoolDataServiceProtocol) {
        self.schoolDataService = schoolDataService
    }
}

extension SATDetailViewModel {
    func fetchSATScore(forSchool id: String) {
        schoolDataService.fetchSATInfo(forSchool: id) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let satScoreInfo):
                    guard let satScoreInfo = satScoreInfo.first else {
                        self?.errorMessage = "No SAT scores found for the given school"
                        return
                    }
                    self?.schoolSATDetail = satScoreInfo
                case .failure(let error):
                    self?.errorMessage = error.localizedDescription
                }
            }
        }
    }
}
