//
//  SATScore.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

struct SATScore: Decodable, Equatable {
    let id: String
    let schoolName: String
    let numberOfTestTakers: String
    let readingScore: String
    let mathScore: String
    let writingScore: String
    
    enum CodingKeys: String, CodingKey {
        
        case id = "dbn"
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
    
    static func == (lhs: SATScore, rhs: SATScore) -> Bool {
        return lhs.id == rhs.id
    }
}
