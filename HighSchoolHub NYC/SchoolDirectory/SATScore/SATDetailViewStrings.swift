//
//  SATDetailViewStrings.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

struct SATDetailViewStrings {
    static var errorLoadingMessage: String {
        return NSLocalizedString("ErrorLoadingMessage", comment: "Error message for loading")
    }
    
    static var loadingMessage: String {
        return NSLocalizedString("LoadingMessage", comment: "Loading message")
    }
}
