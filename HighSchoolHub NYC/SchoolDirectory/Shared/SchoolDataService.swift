//
//  SchoolDataService.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

/*
  SchoolDataService.swift
  Conforms to SchoolDataServiceProtocol, providing methods to fetch school-related data.
  Part of the Data Access Layer, it abstracts the data fetching mechanisms for school entities.
 */
import Foundation

protocol SchoolDataServiceProtocol {
    func fetchSchools(completion: @escaping (Result<[School], NetworkError>) -> Void)
    func fetchSATInfo(forSchool id: String, completion: @escaping (Result<[SATScore], NetworkError>) -> Void)
}

class SchoolDataService: SchoolDataServiceProtocol {
    private let networkService: NetworkServiceProtocol
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func fetchSchools(completion: @escaping (Result<[School], NetworkError>) -> Void) {
        networkService.fetchData(from: API.Endpoints.highSchoolDirectory) { (result: Result<[School], NetworkError>) in
            switch result {
            case .success(let schoolList):
                DispatchQueue.global(qos: .userInitiated).async {
                    let sortedSchools = schoolList.sorted { $0.schoolName < $1.schoolName }
                    
                    DispatchQueue.main.async {
                        completion(.success(sortedSchools))
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    func fetchSATInfo(forSchool id: String, completion: @escaping (Result<[SATScore], NetworkError>) -> Void) {
        let endpoint = "\(API.Endpoints.satScoresInfo)?dbn=\(id)"
        networkService.fetchData(from: endpoint, completion: completion)
    }
}
