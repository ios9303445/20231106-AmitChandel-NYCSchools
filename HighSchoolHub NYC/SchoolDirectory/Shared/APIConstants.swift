//
//  APIConstants.swift
//  HighSchoolHub NYC
//
//  Created by Amit Chandel on 11/5/23.
//

import Foundation

// Defines base URL and endpoints for NYC High School data API.
struct API {
    static let baseURL = "https://data.cityofnewyork.us/resource"
    
    struct Endpoints {
        static let highSchoolDirectory = "\(API.baseURL)/s3k6-pzi2.json"
        static let satScoresInfo = "\(API.baseURL)/f9bf-2cp4.json"
    }
}
