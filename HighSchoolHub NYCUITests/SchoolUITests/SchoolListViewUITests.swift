//
//  SchoolListViewUITests.swift
//  HighSchoolHub NYCUITests
//
//  Created by Amit Chandel on 11/5/23.
//

import XCTest
@testable import HighSchoolHub_NYC

class SchoolListViewUITests: XCTestCase {
    
    var app: XCUIApplication!
    
    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchArguments = ["-UITesting"]
        app.launch()
    }
    
    func testSortButtonExists() throws {
        let sortButton = app.buttons["SortButton"]
        XCTAssertTrue(sortButton.exists, "The sort button should be present on the navigation bar.")
    }
    
    func testSchoolListLoading() throws {
        let loadingIndicator = app.activityIndicators["LoadingProgressView"]
        XCTAssertTrue(loadingIndicator.exists, "The loading indicator should be visible while schools are loading.")
    }
    
    func testSchoolListPopulation() throws {
        let list = app.tables["SchoolList"]
        XCTAssertTrue(list.exists, "The school list should be present on the screen.")
        
        let firstSchool = list.cells.element(boundBy: 0)
        XCTAssertTrue(firstSchool.waitForExistence(timeout: 200), "The first school should appear in the list.")
    }
    
    func testSearchingSchools() throws {
        let searchField = app.searchFields["SearchSchools"]
        XCTAssertTrue(searchField.exists, "The search field should be present on the screen.")
        
        searchField.tap()
        searchField.typeText("Some School Name")
        
        let filteredSchool = app.staticTexts["SchoolRow_SomeSchoolID"]
        XCTAssertTrue(filteredSchool.exists, "The filtered school should appear in the list.")
    }
    
    func testSchoolSelection() throws {
        let list = app.tables["SchoolList"]
        let firstSchool = list.cells.element(boundBy: 0)
        if firstSchool.waitForExistence(timeout: 200) {
            firstSchool.tap()
            let detailView = app.otherElements["SchoolDetailView"]
            XCTAssertTrue(detailView.exists, "The detail view should be displayed after tapping a school.")
        } else {
            XCTFail("The first school did not appear in the list.")
        }
    }
    
    override func tearDownWithError() throws {
        app = nil
    }
}

