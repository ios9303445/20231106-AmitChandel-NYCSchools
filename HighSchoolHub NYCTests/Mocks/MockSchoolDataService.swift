//
//  MockSchoolDataService.swift
//  HighSchoolHub NYCTests
//
//  Created by Amit Chandel on 11/5/23.
//

class MockSchoolDataService: SchoolDataServiceProtocol {
    
    var fetchSchoolsResult: Result<[School], NetworkError>!
    var fetchSATInfoResult: Result<[SATScore], NetworkError>!

    func fetchSchools(completion: @escaping (Result<[School], NetworkError>) -> Void) {
        completion(fetchSchoolsResult)
    }
    
    func fetchSATInfo(forSchool id: String, completion: @escaping (Result<[SATScore], NetworkError>) -> Void) {
        completion(fetchSATInfoResult)
    }
}
