////
////  MockURLSession.swift
////  HighSchoolHub NYCTests
////
////  Created by Amit Chandel on 11/5/23.
////
//
//import Foundation
//
//protocol URLSessionDataTaskProtocol {
//    func resume()
//}
//
//extension URLSessionDataTaskProtocol {
//    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void
//}
//
//protocol URLSessionProtocol {
//    func dataTask(with request: URLRequest, completionHandler: @escaping URLSessionDataTaskProtocol.DataTaskResult) -> URLSessionDataTaskProtocol
//}
//
//class MockURLSession: URLSessionProtocol {
//    var nextDataTask = MockURLSessionDataTask()
//    var nextData: Data?
//    var nextError: Error?
//    var nextResponse: URLResponse?
//
//    func dataTask(with request: URLRequest, completionHandler: @escaping URLSessionDataTaskProtocol.DataTaskResult) -> URLSessionDataTaskProtocol {
//        completionHandler(nextData, nextResponse, nextError)
//        return nextDataTask
//    }
//}
//
//class MockURLSessionDataTask: URLSessionDataTaskProtocol {
//    func resume() {}
//}
//
//extension URLSession: URLSessionProtocol {
//    func dataTask(with request: URLRequest, completionHandler: @escaping URLSessionDataTaskProtocol.DataTaskResult) -> URLSessionDataTaskProtocol {
//        return (dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask) as URLSessionDataTaskProtocol
//    }
//}
//
//extension URLSessionDataTask: URLSessionDataTaskProtocol {}
