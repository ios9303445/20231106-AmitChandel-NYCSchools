//
//  MockNetworkService.swift
//  HighSchoolHub NYCTests
//
//  Created by Amit Chandel on 11/5/23.
//

import XCTest
@testable import HighSchoolHub_NYC

class MockNetworkService: NetworkServiceProtocol {
    
    var fetchSchoolsResult: Result<[School], NetworkError>?
    var fetchSATInfoResult: Result<[SATScore], NetworkError>?
    
    func fetchData<T>(from endpoint: String, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable {
        if let fetchSchoolsResult = fetchSchoolsResult as? Result<T, NetworkError> {
            completion(fetchSchoolsResult)
        } else if let fetchSATInfoResult = fetchSATInfoResult as? Result<T, NetworkError> {
            completion(fetchSATInfoResult)
        }
    }
}
