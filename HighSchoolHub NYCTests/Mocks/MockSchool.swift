//
//  MockSchool.swift
//  HighSchoolHub NYCTests
//
//  Created by Amit Chandel on 11/5/23.
//

import XCTest
@testable import HighSchoolHub_NYC

extension School {
    static func mock(
        id: String = "123",
        schoolName: String = "Test High School",
        website: String = "http://testhighschool.edu",
        neighborhood: String = "Test Neighborhood",
        phoneNumber: String = "123-456-7890",
        totalStudents: String = "1000",
        overviewParagraph: String = "This is a test high school.",
        schoolEmail: String = "info@testhighschool.edu",
        academicOpportunities1: String = "AP Math",
        academicOpportunities2: String = "AP Science",
        extraCurricularActivities: String = "Chess Club, Robotics",
        graduationRate: String = "78%",
        attendanceRate: String = "80%",
        schoolSports: String = "Basketball, Soccer",
        offerRate: String = "85%",
        location: String = "123 Test St, Test City"
    ) -> School {
        return School(
            id: id,
            schoolName: schoolName,
            website: website,
            neighborhood: neighborhood,
            phoneNumber: phoneNumber,
            totalStudents: totalStudents,
            overviewParagraph: overviewParagraph,
            schoolEmail: schoolEmail,
            academicOpportunities1: academicOpportunities1,
            academicOpportunities2: academicOpportunities2,
            extraCurricularActivities: extraCurricularActivities,
            graduationRate: graduationRate,
            attendanceRate: attendanceRate,
            schoolSports: schoolSports,
            offerRate: offerRate,
            location: location
        )
    }
}

