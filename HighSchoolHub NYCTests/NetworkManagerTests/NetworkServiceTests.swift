////
////  NetworkServiceTests.swift
////  HighSchoolHub NYCTests
////
////  Created by Amit Chandel on 11/5/23.
////
//
//import XCTest
//@testable import HighSchoolHub_NYC
//
//final class NetworkServiceTests: XCTestCase {
//
//    var networkService: NetworkService!
//    var mockURLSession: MockURLSession!
//    
//    override func setUp() {
//        super.setUp()
//        mockURLSession = MockURLSession()
//        networkService = NetworkService(session: mockURLSession)
//    }
//
//    override func tearDown() {
//        networkService = nil
//        mockURLSession = nil
//        super.tearDown()
//    }
//}
//
//extension NetworkServiceTests {
//    
//    func testFetchDataSuccess() {
//            // Given
//            let expectedData = "{\"key\":\"value\"}".data(using: .utf8)
//            mockURLSession.nextData = expectedData
//            let url = "http://mockurl.com"
//            
//            let expectation = self.expectation(description: "FetchDataSuccess")
//            
//            // When
//            var fetchedData: [String: String]?
//            var fetchedError: Error?
//            
//            networkService.fetchData(from: url) { (result: Result<[String: String], NetworkError>) in
//                switch result {
//                case .success(let data):
//                    fetchedData = data
//                case .failure(let error):
//                    fetchedError = error
//                }
//                expectation.fulfill()
//            }
//            
//            waitForExpectations(timeout: 1, handler: nil)
//            
//            // Then
//            XCTAssertNotNil(fetchedData)
//            XCTAssertNil(fetchedError)
//            XCTAssertEqual(fetchedData?["key"], "value")
//        }
//        
//        func testFetchDataFailure() {
//            // Given
//            mockURLSession.nextError = NetworkError.badURL
//            let url = "http://mockurl.com"
//            
//            let expectation = self.expectation(description: "FetchDataFailure")
//            
//            // When
//            var fetchedData: [String: String]?
//            var fetchedError: Error?
//            
//            networkService.fetchData(from: url) { (result: Result<[String: String], NetworkError>) in
//                switch result {
//                case .success(let data):
//                    fetchedData = data
//                case .failure(let error):
//                    fetchedError = error
//                }
//                expectation.fulfill()
//            }
//            
//            waitForExpectations(timeout: 1, handler: nil)
//            
//            // Then
//            XCTAssertNil(fetchedData)
//            XCTAssertNotNil(fetchedError)
//            if let error = fetchedError as? NetworkError {
//                switch error {
//                case .badURL:
//                    // Success, this is the expected error
//                    break
//                default:
//                    XCTFail("Expected badURL error")
//                }
//            } else {
//                XCTFail("Error should be of type NetworkError")
//            }
//        }
//}
