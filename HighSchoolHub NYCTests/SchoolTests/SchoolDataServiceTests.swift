//
//  SchoolDataServiceTests.swift
//  HighSchoolHub NYCTests
//
//  Created by Amit Chandel on 11/5/23.
//

import XCTest
@testable import HighSchoolHub_NYC

final class SchoolDataServiceTests: XCTestCase {

    var schoolDataService: SchoolDataService!
    var mockNetworkService: MockNetworkService!
    
    override func setUp() {
        super.setUp()
        mockNetworkService = MockNetworkService()
        schoolDataService = SchoolDataService(networkService: mockNetworkService)
    }

    override func tearDown() {
        mockNetworkService = nil
        schoolDataService = nil
        super.tearDown()
    }

    func testFetchSchoolSuccess() {
        // Given
        let expectedSchools = [School.mock()]
        mockNetworkService.fetchSchoolsResult = .success(expectedSchools)
        
        // When
        var fetchedSchools: [School]?
        let expectation = self.expectation(description: "Fetch Schools Success")
        schoolDataService.fetchSchools { result in
            if case .success(let schoolList) = result {
                fetchedSchools = schoolList
            }
            expectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 1) { _ in
            XCTAssertEqual(expectedSchools, fetchedSchools)
        }
    }
    
    func testFetchSchoolFailure() {
        // Given
        mockNetworkService.fetchSchoolsResult = .failure(.badURL)
        
        // When
        var fetchedError: NetworkError?
        let expectation = self.expectation(description: "Failed to fetch Schools")
        schoolDataService.fetchSchools { result in
            if case .failure(let failure) = result {
                fetchedError = failure
            }
            expectation.fulfill()
        }
        
        // Then
        waitForExpectations(timeout: 1) { _ in
            if let error = fetchedError {
                switch error {
                case .badURL:
                    // Success! We can confirm the error is .badURL
                    break
                default:
                    XCTFail("Expected .badURL error, but got \(error)")
                }
            } else {
                XCTFail("Expected failure, but got no error")
            }
        }
    }
}
