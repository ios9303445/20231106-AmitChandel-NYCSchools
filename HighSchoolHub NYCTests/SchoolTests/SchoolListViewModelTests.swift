//
//  SchoolListViewModelTests.swift
//  HighSchoolHub NYCTests
//
//  Created by Amit Chandel on 11/5/23.
//

import XCTest
@testable import HighSchoolHub_NYC

final class SchoolListViewModelTests: XCTestCase {

    var viewModel: SchoolListViewModel!
    var mockSchoolDataService: MockSchoolDataService!
    
    override func setUp() {
        super.setUp()
        
        mockSchoolDataService = MockSchoolDataService()
        viewModel = SchoolListViewModel(schoolDataService: mockSchoolDataService)
    }

    override func tearDown() {
        viewModel = nil
        mockSchoolDataService = nil
        super.tearDown()
    }
}

extension SchoolListViewModelTests {
    
    func testLoadSchoolsSuccess() {
        // Given
        let expectedSchools = [School.mock()]
        mockSchoolDataService.fetchSchoolsResult = .success(expectedSchools)
        let expectation = self.expectation(description: "Load Schools")

        // When
        viewModel.loadSchools()
        
        // Then
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 2) { error in
            XCTAssertNil(error, "Loading schools took too long.")
            XCTAssertFalse(self.viewModel.isLoading)
            XCTAssertNil(self.viewModel.errorMessage)
            XCTAssertEqual(self.viewModel.schoolList, expectedSchools)
        }
    }
    
    func testLoadSchoolsFailure() {
        // Given
        let expectedError = NetworkError.badURL
        mockSchoolDataService.fetchSchoolsResult = .failure(expectedError)
        let expectation = self.expectation(description: "Load Schools Failure")

        // When
        viewModel.loadSchools()
        
        // Then
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            expectation.fulfill()
        }
        waitForExpectations(timeout: 2) { error in
            XCTAssertNil(error, "Loading schools failure handling took too long.")
            XCTAssertFalse(self.viewModel.isLoading)
            XCTAssertNotNil(self.viewModel.errorMessage)
            XCTAssertTrue(self.viewModel.schoolList.isEmpty)
            XCTAssertEqual(self.viewModel.errorMessage, expectedError.localizedDescription)
        }
    }
}
